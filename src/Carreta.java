/**
 *
 * @author arthur
 */
public class Carreta extends Veiculo{
    public Carreta() {
        this.setCombustivel("Diesel");
        this.setQuedaRendimento(0.0002);
        this.setRendimento(8);
        this.setVelocidadeMedia(60);
        this.setCargaMaxima(30000);
    }
}

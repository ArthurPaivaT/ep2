/**
 *
 * @author arthur
 */
public class Van extends Veiculo{
    public Van() {
        this.setCombustivel("Diesel");
        this.setQuedaRendimento(0.001);
        this.setRendimento(10);
        this.setVelocidadeMedia(80);
        this.setCargaMaxima(3500);
    }
}

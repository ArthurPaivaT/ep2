/**
 *
 * @author arthur
 */
public class Moto extends Veiculo{
    private String combustivel2;
    private double rendimento2;
    private double quedaRendimento2;

    public String getCombustivel2() {
        return combustivel2;
    }

    public void setCombustivel2(String combustivel2) {
        this.combustivel2 = combustivel2;
    }

    public double getRendimento2() {
        return rendimento2;
    }

    public void setRendimento2(float rendimento2) {
        this.rendimento2 = rendimento2;
    }

    public double getQuedaRendimento2() {
        return quedaRendimento2;
    }

    public void setQuedaRendimento2(double quedaRendimento2) {
        this.quedaRendimento2 = quedaRendimento2;
    }

    public Moto() {
        this.setCombustivel("Gasolina");
        this.setQuedaRendimento(0.3);
        this.setRendimento(50);
        this.setVelocidadeMedia(110);
        this.setCargaMaxima(50);
        this.combustivel2 = "Álcool";
        this.quedaRendimento2 = 0.4;
        this.rendimento2 = 43;
    }
}
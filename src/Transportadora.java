/**
 *
 * @author arthur
 */
public class Transportadora {
    
    int nCarros;
    int nCarretas;
    int nVans;
    int nMotos;

    public int getnCarros() {
        return nCarros;
    }

    public void setnCarros(int nCarros) {
        this.nCarros = nCarros;
    }

    public int getnCarretas() {
        return nCarretas;
    }

    public void setnCarretas(int nCarretas) {
        this.nCarretas = nCarretas;
    }

    public int getnVans() {
        return nVans;
    }

    public void setnVans(int nVans) {
        this.nVans = nVans;
    }

    public int getnMotos() {
        return nMotos;
    }

    public void setnMotos(int nMotos) {
        this.nMotos = nMotos;
    }
    
}

public class Carro extends Veiculo{
    private String combustivel2;
    private float rendimento2;
    private double quedaRendimento2;

    public String getCombustivel2() {
        return combustivel2;
    }

    public void setCombustivel2(String combustivel2) {
        this.combustivel2 = combustivel2;
    }

    public float getRendimento2() {
        return rendimento2;
    }

    public void setRendimento2(float rendimento2) {
        this.rendimento2 = rendimento2;
    }

    public double getQuedaRendimento2() {
        return quedaRendimento2;
    }

    public void setQuedaRendimento2(float quedaRendimento2) {
        this.quedaRendimento2 = quedaRendimento2;
    }

    public Carro() {
        this.setCombustivel("Gasolina");
        this.setQuedaRendimento(0.025);
        this.setRendimento(14);
        this.setVelocidadeMedia(100);
        this.setCargaMaxima(360);
        this.combustivel2 = "Álcool";
        this.quedaRendimento2 = 0.0231;
        this.rendimento2 = 12;
    }
    
}

